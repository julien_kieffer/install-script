#Install Python3 & change alias
sudo apt-get python3
alias python=python3

#Install PIP3 & change alias
curl -O http://python-distribute.org/distribute_setup.py
sudo python distribute_setup.py

curl -O https://raw.github.com/pypa/pip/master/contrib/get-pip.py
sudo python3 get-pip.py

alias pip=pip3


#Install python librairies
pip install virtualenv
pip install mongoengine
pip install requests

#Install mongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install mongodb-org

# Install node
apt-get install python-software-properties
apt-add-repository ppa:chris-lea/node.js
apt-get update
sudo apt-get install nodejs npm

#Install ajenti
wget http://repo.ajenti.org/debian/key -O- | apt-key add -
sudo echo "deb http://repo.ajenti.org/ng/debian main main ubuntu" >> /etc/apt/sources.list
sudo apt-get update && apt-get install ajenti -y

#Add mongodb to start
update-rc.d mongodb defaults
